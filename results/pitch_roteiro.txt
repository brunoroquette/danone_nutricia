SLIDE 1 – CAPA
Oá a todos, meu nome é Bruno Roquette, eu sou head de tecnologia da empresa Longevdados e hoje vou falar com vocês sobre o impacto da suplementaçao nutricional na quedas de idosos no Brasil.

SLIDE 2 – CENÁRIO DE QUEDAS NO BRASIL
Hoje o SUS dedica em média, 2% dos seus atendimentos referente a população idosa para eventos de quedas, resultando num custo de 69M /ano. O aumento da probabilidade de queda do idoso cresce exponencialmente com a idade, e os que caem, possuem 34% mais chances de morrer. Os estados com maior prevalência deste evento são sp, mg e rs e 7 a cada 10 são mulheres

SLIDE 3 – PROBLEMA E SOLUÇÃO PROPOSTA
Os fatores de risco que pré dispõe o idoso a queda são diversos e muitas vezes interagem de forma sinérgica entre si. Em virtude do papel já bem conhecido do cálcio e da vitamina D na saúde do sistema musculo esquelético, a nossa proposta visa atuar no reparo da deficiência desses nutrientes, cuja prevalência é grave entre os idosos que caem.
Infelizmente a polifarmácia é comum neste público, por isso pensamos que o veículo desses nutrientes fosse uma opção de alimento que de certa forma está inserido no dia a dia dessas pessoas. A partir disso, se espera que os idosos que utilizarem desse suplemento passarão a ter condições para um melhor desempenho físico, reduzindo assim o risco de quedas, fraturas e consequentemente redução de morte por essas intercorrências.

SLIDE 4 - NEGÓCIO
Estudos revelam que o consumo de produtos lácteos nos últimos 10 anos sofreu um aumento expressivo ao mesmo tempo que a maior parte dos consumidores estarão com 50 anos ou mais na próxima década. A rede varejista poderá atrair mais clientes maduros, sem falar no aproveitamento dos atuais processos de armazenagem e distribuição. 
Além do avanço na área social, a potencial redução de queda em idosos reduzirá também os custos dos serviços de saúde pública. E a simplicidade no enriquecimento do produto permitirá uma oferta a preços muito competitivos frente aos similares da concorrência, tais como: fórmulas e multivitamínicos

SLIDE 5 – IMPACTOS DA SUPLEMENTAÇÃO NA PREVENÇÃO DE QUEDAS
É estimado um público potencial de consumo de 6M idosos com projeção de 8M para 2030, cujo perfil estudado é urbano, consumidor, alfabetizado e com renda acima de 2 salários mínimos
Como resultado, objetiva-se uma redução de 20% nas quedas o que se traduz em economia de 14M / ano ao governo e, principalmente, elevando a qualidade de vida dos idosos e seus familiares. 

SLIDE6 – TIME
Nosso time é composto pela Paula Pereira na área de nutrição, Tatiana Zappa em PeD e Maria Elisa Martins como nossa CEO.
Para mais informações sobre nossa pesquisa, fale com a gente.
Maduros, geraçao do Futuro!

