# LIBRARYS ----------------------------------------------------------------
libs = c('dplyr', 'DiagrammeR')
null = lapply(libs, library, character.only = T)

# INPUT -------------------------------------------------------------------
builderString = "
graph TB
A(Quais são os impactos da suplementacao na prevenção de quedas na pop idosa?)
B[Governo -> Econômicos]
C[Idoso -> Econômicos, Qualidade de vida]
D[Panorâma Atual]
H[datasus-tempo de internacao e custos]
E[Doenças, comorbidades relacionadas as quedas]
I[artigos]
F[População potencialmente benecifiada pela suplementação %]
G[Resultados Previstos, Financeiros e Qualitativos]
A-->B
A-->C
B-->D
D-->H
C-->D
D-->E
D-->I
E-->F
E-->I
F-->G
F-->I
"
# HANDLE ------------------------------------------------------------------
mermaid(builderString)

# OUTPUT ------------------------------------------------------------------
#make manually using view window
